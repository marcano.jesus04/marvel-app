import React from "react";
import { ReactDOM } from "react";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Characters } from './pages/Characters/characters'
import { Comics } from './pages/Comics/comics'
import { Movies } from './pages/Movies/movies'
import { Home } from './pages/Home/home'
import { News } from "./pages/News/news";
import { Navbar } from "./components/Navbar/navbar";

const App = () => {
    return(
        <React.Fragment>
            <Router>
                <Navbar/>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/characters" element={<Characters/>}/>
                    <Route path="/comics" element={<Comics/>}/>
                    <Route path="/movies" element={<Movies/>}/>
                    <Route path="/news" element={<News/>}/>
                </Routes>
            </Router>
        </React.Fragment>
    )
}

export default App;