import React, {useState} from 'react'
import '../../assets/components/navbar.css'
import * as GoIcons from 'react-icons/go' 

function Navbar() {

  const [topBarContent, SetTopBarContent] = useState(false);

  const showTopBarContent = () => SetTopBarContent(!topbar)


  return (
    <>
      <div className='nav-container'>
        <div className='top-content'>
          <div className='top-login'>Login</div>
          <div className='top-logo'></div>
          <div className='top-search-promo'>
            <div className='top-suscription'>
              <img className='promo-image' src="https://i.annihil.us/u/prod/marvel/images/mu/web/2021/icon-mu-shield.png" alt="Marvel Unlimited logo"></img>
              <div className='mu-text'>
                <p className='mu-title'>MARVEL UNLIMITED</p>
                <p className='mu-subtitle'>SUBSCRIBE</p>
              </div>
            </div>
            <div className='top-search'><GoIcons.GoSearch/></div>
          </div>
        </div>
        <div className='navegate-items'>  </div>
      </div>
    </>
  )
}

export { Navbar }
